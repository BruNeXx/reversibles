var reversible = require('..')
var test = require('tap').test;

test('Test isReversible', function (t) {
    t.plan(20);
    t.notOk(reversible.isReversible(1));
    t.notOk(reversible.isReversible(2));
    t.notOk(reversible.isReversible(3));
    t.notOk(reversible.isReversible(4));
    t.notOk(reversible.isReversible(5));
    t.notOk(reversible.isReversible(6));
    t.notOk(reversible.isReversible(7));
    t.notOk(reversible.isReversible(8));
    t.notOk(reversible.isReversible(9));
    t.notOk(reversible.isReversible(10));
    t.notOk(reversible.isReversible(20));
    t.notOk(reversible.isReversible(30));
    t.notOk(reversible.isReversible(50));
    t.notOk(reversible.isReversible(70));
    t.notOk(reversible.isReversible(90));
    t.notOk(reversible.isReversible(100));
    t.ok(reversible.isReversible(36));
    t.ok(reversible.isReversible(63));
    t.ok(reversible.isReversible(904));
    t.notOk(reversible.isReversible(885));
});
