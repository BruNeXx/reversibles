var reversible = require('..')
var test = require('tap').test;

test('Test listReversibles', function (t) {
    t.plan(1);
    t.equals(120, reversible.listReversibles(1000).length);
});
