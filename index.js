//** function to check if a number is reversible
var isReversible = module.exports.isReversible =  function(number){
  if(number/10 < 1) return false; //number is less than 10
  var reversedNumber = reverseNumber(number);
  if(reversedNumber.split('')[0] === '0') return false; //if the reversed number starts with zero then is not reversible...
  var result = number + parseInt(reversedNumber);

  var isReversible = false;
  var resultArray = result.toString().split('');
  for (i = 0, iMax = resultArray.length; i < iMax; i += 1) {
    if(isOdd(parseInt(resultArray[i]))){
      isReversible = true;
    }else {
      isReversible = false
      break;
    };
  }
  return isReversible;
};

//** list all reversibles, returns a array with all reversibles
var listReversibles = module.exports.listReversibles =   function(upperLimit){
  var reversibles = [];
  for (var x = 1; x<=upperLimit; x++){
    if(isReversible(x) === true) {
      reversibles.push(x);
    }
  }
  return reversibles;
}

//** reverses a number
var reverseNumber = module.exports.reverseNumber =  function(number){
  return number.toString().split('').reverse().join('');
}

//** check if a number is odd
var isOdd = module.exports.isOdd =  function(number){
  if (number%2 != 0){
    return true;
  }else{
    return false;
  }
}
